const trash = require('trash');
const globby = require('globby');

const paths = globby.sync([
    'app/**/*.js',
    'app/**/*.css',
    'app/**/*.map',
    'dist',
    'packages/**/*.js',
    '!packages/**/*.mockData.generator.js',
    '!packages/**/cookie-policy/lib/scripts/*.js',
    'packages/**/*.map'
]);

function cleanProject() {
    return Promise.all(paths.map(path => trash(path))).then(() => console.log('Project clean')).catch(err => console.error(err));
}

cleanProject();