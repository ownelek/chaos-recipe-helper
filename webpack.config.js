let webpack = require('webpack');
let path = require('path');
let CircularDependencyPlugin = require('circular-dependency-plugin');

let plugins = [];

plugins.push(
    new webpack.DefinePlugin({
        "global": "window",
        'process.env': {
            'NODE_ENV': '"development"'
        }
    })
);

plugins.push(new CircularDependencyPlugin({
    exclude: /node_modules/,
    failOnError: true
}));

module.exports = {
    context: __dirname,
    entry: "./app/client.js",
    devtool: "source-map",
    output: {
        publicPath: '/assets/js/',
        path: path.join(__dirname, 'app/assets/js/'),
        filename: 'app.js',
        sourceMapFilename: 'app.js.map'
    },
    node : {
        console: false,
        global: false,
        process: false,
        Buffer: false,
        __filename: false,
        __dirname: false,
        setImmediate: false
    },
    plugins: plugins
};
