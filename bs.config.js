// browser-sync configuration

module.exports = {
    files: ["app/assets/**/*.*"],
    proxy: "localhost:8000",
    open: true,
    timestamps: true,
    fileTimeout: 2000,
    reloadDelay: 2000,
    reloadDebounce: 2000
};