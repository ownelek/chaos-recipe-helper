import {createElement} from 'inferno-create-element';

export function indexPage(): JSX.Element {
    return (
        <html>
        <head>
            <title>Chaos Recipe Helper</title>
            <meta charSet='utf-8'/>
            <link rel="stylesheet" href="assets/css/index.css"/>
            <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.1/css/font-awesome.min.css"/>
        </head>
        <body>
        <main/>
        <script type='text/javascript' src='assets/js/app.js'/>
        </body>
        </html>
    );
}
