import * as Inferno from "inferno";
import {createElement} from 'inferno-create-element';
import {connect} from 'inferno-redux';
import {ChaosHelperState} from "../store";
import {loadStashesAction} from "../actions/loadStashes.action";
import {Stash} from "../../../common/entities/stash";
import {bind} from 'decko';
import {Profile} from "../../../common/entities/profile";
import {saveProfileAction} from "../actions/saveProfile.action";
import {loadProfileAction} from "../actions/loadProfile.action";
import {loadStashItemsAction} from "../actions/loadStashItems.action";
import {sleep} from "../../../common/sleep";
import {StashItemCategory, StashItemRarity} from "../../../common/entities/stashItem";

const ONE_MINUTE_INTERVAL: number = 60 * 1000;

enum ItemCategory {
    Helmet,
    BodyArmour,
    Gloves,
    Boots,
    Belt,
    Amulet,
    Ring,
    Weapon
}

function safeGetNumber(n: number | undefined | null): number {
    if(n == null) {
        return 0;
    }

    return n;
}

export class ChaosHelperViewComponent extends Inferno.Component<ChaosHelperState, {}> {
    private poeSessIdInput: HTMLInputElement;
    private accountNameInput: HTMLInputElement;
    private leagueInput: HTMLSelectElement;
    private checkItemsIntervalIndex: number;

    public componentDidMount(): void {
        loadProfileAction();
    }

    public componentDidUpdate(): void {
        if(this.props.profile && this.props.stashes && this.props.stashes.length > 0 && !this.checkItemsIntervalIndex) {
            this.loadStashItems();
            this.checkItemsIntervalIndex = window.setInterval(this.loadStashItems, ONE_MINUTE_INTERVAL);
        }
    }

    public componentWillUnmount(): void {
        window.clearInterval(this.checkItemsIntervalIndex);
        this.checkItemsIntervalIndex = null;
    }

    public render(): JSX.Element {
        let profile: Profile = this.props.profile;

        let itemsCounts: Map<StashItemCategory, number> = this.props.stashes.reduce((countsMap, stash) => {
            return stash.items.reduce((itemsCountsMap, item) => {
                let itemCount: number = itemsCountsMap.get(item.category) || 0;

                if (item.rarity === StashItemRarity.Rare) {
                    itemsCountsMap.set(item.category, itemCount + 1);   
                }

                return itemsCountsMap;
            }, countsMap);
        }, new Map());

        let categoryWeights: Map<ItemCategory, number> = new Map();

        let oneHandedWeaponsCount: number = safeGetNumber(itemsCounts.get(StashItemCategory.OneHandedWeapon));
        let oneHandedWeaponsCombinations: number = Math.min(oneHandedWeaponsCount, safeGetNumber(itemsCounts.get(StashItemCategory.Shield)));

        if(oneHandedWeaponsCombinations < oneHandedWeaponsCount) {
            oneHandedWeaponsCombinations += (oneHandedWeaponsCount - oneHandedWeaponsCombinations) / 2;
        }

        categoryWeights.set(ItemCategory.Helmet, safeGetNumber(itemsCounts.get(StashItemCategory.Helmet)));
        categoryWeights.set(ItemCategory.BodyArmour, safeGetNumber(itemsCounts.get(StashItemCategory.BodyArmour)));
        categoryWeights.set(ItemCategory.Gloves, safeGetNumber(itemsCounts.get(StashItemCategory.Gloves)));
        categoryWeights.set(ItemCategory.Boots, safeGetNumber(itemsCounts.get(StashItemCategory.Boots)));
        categoryWeights.set(ItemCategory.Belt, safeGetNumber(itemsCounts.get(StashItemCategory.Belt)));
        categoryWeights.set(ItemCategory.Amulet, safeGetNumber(itemsCounts.get(StashItemCategory.Amulet)));
        categoryWeights.set(ItemCategory.Ring, safeGetNumber(itemsCounts.get(StashItemCategory.Ring)) / 2);
        categoryWeights.set(ItemCategory.Weapon,
            (oneHandedWeaponsCombinations +
                safeGetNumber(itemsCounts.get(StashItemCategory.TwoHandedWeapon)) +
                safeGetNumber(itemsCounts.get(StashItemCategory.Bow))
            )
        );


        let possibleChaosRecipes: number = Math.min(...categoryWeights.values());

        let categoryWeightsArray: Array<[ItemCategory, number]> = Array.from(categoryWeights.entries());
        categoryWeightsArray.sort((a, b) => a[1] - b[1]);

        return (
            <div className='container'>
                <div className='row'>
                    <div className='col text-center'>
                        <div className='jumbotron'>
                            <h1 className='display-4'>Chaos Recipe Helper</h1>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-6'>
                        <div className='row border-bottom'>
                            <div className='col'>
                                <h3>Profile</h3>
                                <form onSubmit={this.loadStashes}>
                                    <div className='form-group'>
                                        <label for='poeSessId'>POESESSID</label>
                                        <input type='text' id='poeSessId' className='form-control' ref={element => this.poeSessIdInput = element} defaultValue={profile.poeSessId}/>
                                    </div>
                                    <div className='form-group'>
                                        <label for='accountName'>Account Name</label>
                                        <input id='accountName' className='form-control' type='text' ref={element => this.accountNameInput = element} defaultValue={profile.accountName}/>
                                    </div>
                                    <div className='form-group'>
                                        <label for='league'>League</label>
                                        <select id='league' className='form-control' ref={element => this.leagueInput = element}>
                                            <option value='Betrayal' defaultChecked={profile.league === 'Betrayal'}>Betrayal</option>
                                            <option value='Standard' defaultChecked={profile.league === 'Standard'}>Standard</option>
                                        </select>
                                    </div>
                                    <input type='submit' className='btn btn-primary' value='Get stashes'/>
                                </form>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='col'>
                                <p>Stashes to watch:</p>
                                {this.props.stashes && this.props.stashes.length > 0 ?
                                    this.props.stashes.map(stash =>
                                        <StashItem stash={stash}
                                                   checked={profile.watchedStashesIds.indexOf(stash.id) >= 0}
                                                   onStashSelect={this.stashSelected}/>) :

                                    <div className='alert alert-warning'>No stashes yet</div>
                                }
                            </div>
                        </div>
                    </div>
                    <div className='col-6'>
                        <table className={`table ${categoryWeightsArray.length === 0 ? 'd-none' : ''}`}>
                            <thead>
                            <tr>
                                <th scope='col'>Item Type</th>
                                <th scope='col'>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            {categoryWeightsArray.map(entry =>
                                <tr>
                                    <td>{ItemCategory[entry[0]]}</td>
                                    <td>{entry[1]}</td>
                                </tr>
                            )}
                            <tr>
                                <td><b>Chaos recipes</b></td>
                                <td><b>{possibleChaosRecipes}</b></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }

    @bind
    private async loadStashes(event: Event): Promise<void> {
        event.preventDefault();

        if(!window.localStorage.getItem('profile') || this.props.profile.poeSessId !== this.poeSessIdInput.value) {
            saveProfileAction(new Profile(this.poeSessIdInput.value, this.accountNameInput.value, this.leagueInput.value, []));
        }

        await loadStashesAction(this.poeSessIdInput.value, this.accountNameInput.value, this.leagueInput.value);
    }

    @bind
    private stashSelected(stash: Stash, checked: boolean): void {
        let profile: Profile = this.props.profile;

        if(checked) {
            if(profile.watchedStashesIds.indexOf(stash.id) === -1) {
                profile.watchedStashesIds.push(stash.id);
            }
        } else {
            let idx: number = profile.watchedStashesIds.indexOf(stash.id);

            if(idx >= 0) {
                profile.watchedStashesIds.splice(idx, 1);
            }
        }

        saveProfileAction(profile);
    }

    @bind
    private async loadStashItems(): Promise<void> {
        let checkedStashes: Array<Stash> = this.props.stashes.filter(stash => this.props.profile.watchedStashesIds.indexOf(stash.id) >= 0);

        console.log(checkedStashes);
        for(let stash of checkedStashes) {
            await loadStashItemsAction(this.props.profile, stash);
            await sleep(1000);
        }
    }
}

function StashItem({ stash, checked, onStashSelect }: { stash: Stash, checked: boolean, onStashSelect: (stash: Stash, checked: boolean) => void }): JSX.Element {
    function stashSelectedFactory(event: Event): void {
        onStashSelect(stash, (event.currentTarget as HTMLInputElement).checked);
    }

    return (
        <div className='form-check'>
            <input className='form-check-input' type='checkbox' checked={checked} onChange={stashSelectedFactory}/>
            <label className='form-check-label'>{stash.name} {stash.type}</label>
        </div>
    );
}

function mapStateToProps(state: ChaosHelperState): any {
    let emptyProfile: Profile = new Profile('', '', '', []);
    return {
        stashes: state ? state.stashes || [] : [],
        profile: state ? state.profile || emptyProfile : emptyProfile
    };
}

export const ChaosHelperView: typeof ChaosHelperViewComponent = connect(mapStateToProps)(ChaosHelperViewComponent);