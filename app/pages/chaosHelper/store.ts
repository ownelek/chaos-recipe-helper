import {createStore, combineReducers, Reducer} from "redux";
import {Stash} from "../../common/entities/stash";
import {Profile} from "../../common/entities/profile";

export interface ChaosHelperState {
    stashes: Array<Stash>;
    profile: Profile;
}

export const stashesReducer: Reducer<Array<Stash>> = (state, action) => {
    switch(action.type) {
        case 'UPDATE_STASHES': return action.stashes;
        case 'UPDATE_STASH_ITEMS': {
            action.stash.items = action.items;
            return [...state];
        }
        default: return state || [];
    }
};

export const profileReducer: Reducer<Profile> = (state, action) => {
    switch(action.type) {
        case 'UPDATE_PROFILE': return action.profile;
        default: return state || null;
    }
};

export const chaosHelperStore = createStore(combineReducers({
    stashes: stashesReducer,
    profile: profileReducer
}));
