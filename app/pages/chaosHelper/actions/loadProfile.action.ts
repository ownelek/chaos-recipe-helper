import {Profile} from "../../../common/entities/profile";
import {EntitySerializer} from "../../../common/entities/entitySerializer";
import {chaosHelperStore} from "../store";
import {Stash} from "../../../common/entities/stash";

export function loadProfileAction(): void {
    // Ensure serialization
    EntitySerializer.addSerializable(Stash);

    let serializedProfile: any = JSON.parse(window.localStorage.getItem('profile'));

    if(serializedProfile == null) {
        chaosHelperStore.dispatch({
            type: 'UPDATE_PROFILE',
            profile: new Profile('', '', '', [])
        });

        return;
    }

    let profile: Profile = EntitySerializer.deserialize(serializedProfile);

    chaosHelperStore.dispatch({
        type: 'UPDATE_PROFILE',
        profile
    });

    console.log('Loaded profile from localStorage');
}