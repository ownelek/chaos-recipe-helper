import {Profile} from "../../../common/entities/profile";
import {EntitySerializer} from "../../../common/entities/entitySerializer";
import {chaosHelperStore} from "../store";

export function saveProfileAction(profile: Profile): void {
    EntitySerializer.addSerializable(Profile);
    window.localStorage.setItem('profile', JSON.stringify(EntitySerializer.serialize(profile)));

    // to ensure profile is properly set in state
    chaosHelperStore.dispatch({
        type: 'UPDATE_PROFILE',
        profile: new Profile(profile.poeSessId, profile.accountName, profile.league, profile.watchedStashesIds)
    });
}
