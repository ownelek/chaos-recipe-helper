import {Profile} from "../../../common/entities/profile";
import {Stash} from "../../../common/entities/stash";
import {chaosHelperStore} from "../store";
import {EntitySerializer, SerializedEntity} from "../../../common/entities/entitySerializer";
import {StashItem} from "../../../common/entities/stashItem";

export async function loadStashItemsAction(profile: Profile, stash: Stash): Promise<void> {
    EntitySerializer.addSerializable(StashItem);

    let response: Response = await fetch(`/api/stashes/items?poeSessId=${profile.poeSessId}&accountName=${profile.accountName}&league=${profile.league}&stashIndex=${stash.index}`);

    let serializedItems: Array<SerializedEntity<StashItem>> = await response.json();

    chaosHelperStore.dispatch({
        type: 'UPDATE_STASH_ITEMS',
        stash,
        items: serializedItems.map(serializedStash => EntitySerializer.deserialize(serializedStash))
    });
}