import {Stash} from "../../../common/entities/stash";
import {EntitySerializer, SerializedEntity} from "../../../common/entities/entitySerializer";
import {chaosHelperStore} from "../store";

export async function loadStashesAction(poeSessId: string, accountName: string, league: string): Promise<void> {
    // Ensure serialization
    EntitySerializer.addSerializable(Stash);

    let response: Response = await fetch(`/api/stashes?poeSessId=${poeSessId}&accountName=${accountName}&league=${league}`);

    let serializedStashes: Array<SerializedEntity<Stash>> = await response.json();

    chaosHelperStore.dispatch({
        type: 'UPDATE_STASHES',
        stashes: serializedStashes.map(serializedStash => EntitySerializer.deserialize(serializedStash))
    });
}