import * as Inferno from "inferno";
import {createElement} from 'inferno-create-element';
import {ChaosHelperView} from "./pages/chaosHelper/ui/ChaosHelperView";
import {Provider} from 'inferno-redux';
import {chaosHelperStore} from "./pages/chaosHelper/store";

Inferno.render((
    <Provider store={chaosHelperStore}>
        <ChaosHelperView/>
    </Provider>
), document.getElementsByTagName('main')[0]);
