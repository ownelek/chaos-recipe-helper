import {Module} from "@nestjs/common";
import {StashController} from "./stash.controller";

@Module({
    components: [],
    controllers: [StashController]
})
export class ApplicationModule {}