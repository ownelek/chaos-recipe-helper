import {Controller, Get, Param, Query} from "@nestjs/common";
import {Stash} from "../common/entities/stash";
import {EntitySerializer, SerializedEntity} from "../common/entities/entitySerializer";
import nodeFetch, {Response as FetchResponse} from 'node-fetch';
import {resolveStashItemCategory, StashItem} from "../common/entities/stashItem";

interface GetStashItemsResponse {
    numTabs: number;
    tabs?: Array<any>;
    items?: Array<any>;
}

@Controller('stashes')
export class StashController {
    @Get()
    public async getStashes(@Query('poeSessId') poeSessId: string, @Query('accountName') accountName: string, @Query('league') league: string): Promise<Array<SerializedEntity<Stash>>> {
        let rawStashes: GetStashItemsResponse = await this.callGetStashItems(poeSessId, accountName, league, true, -1);

        let stashes: Array<Stash> = rawStashes.tabs.map(rawStash => new Stash(rawStash.id, rawStash.n, rawStash.i, rawStash.type, []));

        return stashes.map(stash => EntitySerializer.serialize(stash));
    }

    @Get('items')
    public async getStashItems(
        @Query('poeSessId') poeSessId: string,
        @Query('accountName') accountName: string,
        @Query('league') league: string,
        @Query('stashIndex') stashIndex: string): Promise<Array<SerializedEntity<StashItem>>> {

        let rawItems: GetStashItemsResponse = await this.callGetStashItems(poeSessId, accountName, league, false, parseInt(stashIndex, 10));

        let items: Array<StashItem> = rawItems.items.map(
            rawItem => new StashItem(rawItem.id, rawItem.name, rawItem.ilvl, rawItem.frameType, rawItem.identified, resolveStashItemCategory(rawItem.category))
        );

        return items.map(item => EntitySerializer.serialize(item));
    }

    private async callGetStashItems(poeSessId: string, accountName: string, league: string, tabs: boolean, tabIndex: number): Promise<GetStashItemsResponse> {
        let response: FetchResponse = await nodeFetch(
            `https://pathofexile.com/character-window/get-stash-items?league=${league}&tabs=${tabs ? '1' : '0'}&tabIndex=${tabIndex}&accountName=${accountName}`, {
                headers: {
                    'Cookie': `POESESSID=${poeSessId}`
                }
            }
        );

        return await response.json();
    }
}
