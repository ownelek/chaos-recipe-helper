import {NestFactory} from '@nestjs/core';
import {ApplicationModule} from './api/app.module';
import {renderToString} from 'inferno-server';
import {indexPage} from "./index";
import * as morgan from 'morgan';
import * as express from 'express';

async function bootstrap() {
    let port: string = process.env.PORT || '8000';
    const app = await NestFactory.create(ApplicationModule);

    app.setGlobalPrefix('api');

    app.use(morgan('dev'));

    app.use(express.static(__dirname));

    await app.listen(port);

    app.use((req: express.Request, res: express.Response) => {
        res.send(renderToString(indexPage()));
    });
}

bootstrap();
