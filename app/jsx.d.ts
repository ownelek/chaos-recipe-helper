import {VNode} from 'inferno';

declare global {
    module JSX {
        interface Element extends VNode {}
    }
}
