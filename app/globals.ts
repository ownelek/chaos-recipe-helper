interface Newable<T> {
    new (...args: Array<any>): T;
}
