import {StashItem} from "./stashItem";
import {EntitySerializer} from "./entitySerializer";

export class Stash {
    constructor(
        public id: string,
        public name: string,
        public index: number,
        public type: string,
        public items?: Array<StashItem>
    ) {}
}

EntitySerializer.addSerializable(Stash);
