export interface SerializedEntity<T> {
    name: string;
    entity: T;
}

export class EntitySerializer {
    public static serializables: Map<string, Newable<any>> = new Map();

    public static addSerializable<T>(entityClass: Newable<T>): void {
        console.log("Added", entityClass.name);
        this.serializables.set(entityClass.name, entityClass);
    }

    public static serialize<T>(entity: T): SerializedEntity<T> {
        return {
            name: entity.constructor.name,
            entity: entity
        };
    }

    public static deserialize<T>(serializedEntity: SerializedEntity<T>): T {
        let entityClass: Newable<T> = this.serializables.get(serializedEntity.name);
        let entity: T = new entityClass();

        return Object.assign(entity, serializedEntity.entity);
    }
}
