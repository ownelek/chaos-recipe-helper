import {EntitySerializer} from "./entitySerializer";

export class Profile {
    constructor(
        public poeSessId: string,
        public accountName: string,
        public league: string,
        public watchedStashesIds: Array<String>
    ) {}
}

EntitySerializer.addSerializable(Profile);
