import {EntitySerializer} from "./entitySerializer";

export enum StashItemRarity {
    Normal = 0,
    Magic = 1,
    Rare = 2,
    Unique = 3
}

export enum StashItemCategory {
    Amulet,
    Ring,
    Belt,
    BodyArmour,
    Helmet,
    Gloves,
    Boots,
    OneHandedWeapon,
    TwoHandedWeapon,
    Bow,
    Quiver,
    Shield,

    Other
}

export interface StashItemRawCategory {
    weapons?: [string];
    armour?: [string];
    accessories?: [string];
}

export function resolveStashItemCategory(rawCategory: StashItemRawCategory): StashItemCategory {
    if(rawCategory.weapons) {
        switch(rawCategory.weapons[0]) {
            case 'onesword':
            case 'oneaxe':
            case 'onemace':
            case 'dagger':
            case 'claw':
            case 'sceptre':
            case 'wand':
                return StashItemCategory.OneHandedWeapon;

            case 'twosword':
            case 'twoaxe':
            case 'twomace':
            case 'staff':
                return StashItemCategory.TwoHandedWeapon;

            case 'bow':
                return StashItemCategory.Bow;
        }
    } else if(rawCategory.armour) {
        switch(rawCategory.armour[0]) {
            case 'chest':
                return StashItemCategory.BodyArmour;
            case 'quiver':
                return StashItemCategory.Quiver;
            case 'shield':
                return StashItemCategory.Shield;
            case 'gloves':
                return StashItemCategory.Gloves;
            case 'boots':
                return StashItemCategory.Boots;
            case 'helmet':
                return StashItemCategory.Helmet;
        }
    } else if(rawCategory.accessories) {
        switch(rawCategory.accessories[0]) {
            case 'belt':
                return StashItemCategory.Belt;
            case 'amulet':
                return StashItemCategory.Amulet;
            case 'ring':
                return StashItemCategory.Ring;
        }
    }

    return StashItemCategory.Other;
}

export class StashItem {
    constructor(
        public id: string,
        public name: string,
        public itemLevel: number,
        public rarity: StashItemRarity,
        public identified: boolean,
        public category: StashItemCategory
    ) {}
}

EntitySerializer.addSerializable(StashItem);